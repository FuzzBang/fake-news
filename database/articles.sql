-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2018 at 11:52 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fake_news`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `URL` varchar(250) NOT NULL,
  `Fake` int(11) NOT NULL,
  `Legit` int(11) NOT NULL,
  `Closed` bit(1) NOT NULL,
  `DateResolved` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`URL`, `Fake`, `Legit`, `Closed`, `DateResolved`) VALUES
('beforeitsnews.com/', 1, 0, b'1', '2018-01-22 14:19:19'),
('bizstandardnews.com', 1, 0, b'1', '2018-01-22 14:19:19'),
('Bloomberg.ma', 1, 0, b'1', '2018-01-22 14:19:19'),
('cnn-trending.com', 1, 0, b'1', '2018-01-22 14:19:19'),
('conservative101.com/\r\n', 1, 0, b'1', '2018-01-22 14:19:19'),
('CountyNewsroom.info', 1, 0, b'1', '2018-01-22 14:19:19'),
('https://70news.wordpress.com/', 1, 0, b'1', '2018-01-22 14:19:19'),
('https://en.wikipedia.org/wiki/LAMP_(software_bundle)', 1, 2, b'1', '2018-01-22 15:23:35'),
('https://support.microsoft.com/en-gb/help/12373/windows-update-faq', 0, 1, b'0', '2018-01-23 10:47:35'),
('https://www.archlinux.org/', 9, 6, b'0', '2018-01-23 10:46:22'),
('https://www.bbc.co.uk/', 0, 1, b'1', '2018-01-22 14:19:19'),
('https://www.ft.com/content/783a0840-ff71-11e7-9650-9c0ad2d7c5b5', 0, 101, b'1', '2018-01-22 15:06:05'),
('https://www.google.co.uk/search?q=facebook&oq=face&aqs=chrome.1.69i57j35i39j69i61l3j0.1121j0j7&sourceid=chrome&ie=UTF-8', 0, 3, b'0', '2018-01-23 10:41:29'),
('https://www.infowars.com/great-again-jobless-claims-in-u-s-plunge-to-lowest-weekly-tally-since-1973/', 102, 3, b'1', '2018-01-22 14:19:19'),
('https://www.manchestereveningnews.co.uk/news/greater-manchester-news/yobs-smash-car-barton-bridge-14186526', 200, 150, b'1', '2018-01-22 16:08:25'),
('https://www.thetimes.co.uk/edition/news/boris-johnson-demands-5bn-extra-for-nhs-hsg0cqj3r', 0, 3, b'0', '2018-01-23 10:33:47'),
('https://www.w3schools.com/tags/tag_img.asp', 4, 2, b'0', '2018-01-23 10:35:37'),
('www.celebtricity.com/\r\n', 1, 0, b'1', '2018-01-22 14:19:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`URL`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
