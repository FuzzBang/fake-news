<!DOCTYPE html>
<html>
<?php
require('database/database.php')
?>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/master.css">

	<title></title>
	<meta name="description" content="" />
	<meta name="author" content="" />
	<link rel="icon" href="images/favicon.ico" />

</head>
<body>
	<header class="col-lg-12">
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse" style="top: 56px">
				<ul class="navbar-nav mx-auto">
					<li class="nav-item" id="home">
						<a class="nav-link" href="index.html">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="FakeToday.html">Today</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="detecting.html">Detect</a>
					</li>
					<a class="navbar-brand" href="index.html"><img src="images/logo.png" alt="Logo" style="height: 42px;"></a>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" id="dropdown1" data-toggle="dropdown" href="#">Impacts</a>
						<div class="dropdown-menu" aria-labelledby="dropdown02">
							<a class="dropdown-item" href="impact.html">Business</a>
							<a class="dropdown-item" href="impactPolitics.html">Politics</a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="search.php">Search</a>
					</li>
				</ul>
			</div>
		</nav>
		<div class="jumbotron">
			<div class="container mx-auto">
				<h1 class="display-3">Article Checker</h1>
				<p>Includes website checker to determing if it is nutorious for fake news.</p>
				<form class="form mx-auto" method="post" style="max-width: 500px">
					<div class="input-group">
						<input type="text" class="form-control" id="searchbar" placeholder="Search" name="url" aria-label="Search" aria-describedby="basic-addon1" style="max-width:420px;" />
						<div class="input-group-prepend">
							<button class="btn my-2 my-sm-0" type="submit" id="basic-addon1" style="background-color: #c83e26; color: white;">Check</button>
						</div>
					</div>
				</form>
			</div>
  		</div>
	</header>
	<main>
		<?php
		$fakeresult = "ERROR"; //Define result variable
		if (isset($_POST["url"])) { //Check that the URL was posted
			  $url = (parse_url($_POST["url"])); //Parse string into URL

			  if (isset($url["host"])) { //Get domain name from URL
				  $sql = "SELECT URL, Fake, Legit, Closed FROM articles WHERE URL LIKE '%" . $url["host"] . "%';"; //Get results of article posted
				  $result = $conn->query($sql); //Execute query and store in $result

				  if ($result->num_rows > 0) { //If article is in are database
				      while($row = $result->fetch_assoc()) { //Get next row
				          if ($row["Closed"] == 1) { //If voting is over
				            if ($row["Fake"] >= $row["Legit"]) { //If more fake votes that trusted votes
				              $fakeresult = "Fake"; //Result of URL is fake
				            } else if ($row["Legit"] > $row["Fake"]) { //If more trusted votes than fake votes
				              $fakeresult = "Trusted"; //Result of URL is trusted
				            }
				          } else {
				              $fakeresult = "Voting"; //If voting is still open then result is unkown yet
				          }
				      }
				  } else {
				    $fakeresult = "Voting"; //Unkown to our database so open to voting
				  }

					  if (isset($_POST['vote']) && isset($_POST['url'])) { //If the user has voted for a URL
									    if ($_POST['vote'] == 'trusted') { //If they voted trusted then increment Legit column, add to database if does not exist
									          $sql = "INSERT INTO articles (URL, Fake, Legit, Closed) VALUES('".$_POST['url']."', 0, 1, 0) ON DUPLICATE KEY UPDATE Legit = Legit + 1";
									    } elseif ($_POST['vote'] == 'fake') { //If they voted fake then increment Fake column, add to database if does not exist
									          $sql = "INSERT INTO articles (URL, Fake, Legit, Closed) VALUES('".$_POST['url']."', 1, 0, 0) ON DUPLICATE KEY UPDATE Fake = Fake + 1";
									    }
					    $conn->query($sql);

							//Add IP to list of computers which have already voted for this URL
							$sql = "INSERT INTO votes (URL, IP) VALUES('".$_POST['url']."','". $_SERVER['REMOTE_ADDR']."');";
							$conn->query($sql);

							//If the threshold of 1 votes is reached, close the voting
							$sql = "UPDATE articles SET Closed = 1 WHERE URL = '".$_POST['url']."' AND Closed = 0 AND (Fake + Legit) = 1";
							$conn->query($sql);
					  }

				   echo "<div id='content'>";
				   	echo "<div class='row offset-lg-2 col-lg-8 col-sm-12'>";
				   		echo "<div class='col-lg-4 order-lg-2 panel panel-default'>";
				   			echo "<div class='panel-heading'>";
				   				echo "<h2>Verdict:</h2>";
				   			echo "</div>";
				   			echo "<div class='panel-body'>";

									 //Output the verdict of a URL
								     echo "<h1><span class='col-lg-12 badge badge-";
								     if ($fakeresult == 'Trusted') {
								       $status = 'success'; //If trusted make badge (verdict result) green
								     } elseif ($fakeresult == "Fake") {
								      $status = 'danger'; //If fake make badge red
								     } else {
								       $status = 'warning'; //If voting make badge yellow
								     } echo $status."'> $fakeresult</span></h1>";

								   	if ($fakeresult == "Voting") { //If URL on vote show voting buttons
											$sql = "SELECT IP FROM votes WHERE URL = '".$_POST['url']."' AND IP = '".$_SERVER['REMOTE_ADDR'] ."';";
											$ip = $conn->query($sql); //Get if user has already voted on this URL

											if ($ip->num_rows > 0) { //If they have voted already, disable buttons
												echo "<div id='vote' class='row'>";
													echo "<button class='btn btn-success col-lg-5 disabled voted' data-toggle='tooltip' data-placement='bottom' title='You have already voted'><i class='fas fa-thumbs-up'></i> Trusted</button>";
													echo "<button class='btn btn-danger offset-lg-2 col-lg-5 disabled voted' data-toggle='tooltip' data-placement='bottom' title='You have already voted'><i class='fas fa-thumbs-down'></i> Fake</button>";
												echo "</div>";
											} else { //If they have not voted already, enable buttons
												echo "<form method='post' action='search.php'>"; //When they vote post result and reload page
													echo "<div id='vote' class='row'>";
														echo "<input type='hidden' name='url' id='hiddenurl' value='".$_POST['url']."'/>"; //Sends URL voted on behind the scenes
													    echo "<button type='submit' name='vote' value='trusted' class='btn btn-success col-lg-5'><i class='fas fa-thumbs-up'></i> Trusted</button>";
													    echo "<button type='submit' name='vote' value='fake' class='btn btn-danger offset-lg-2 col-lg-5'><i class='fas fa-thumbs-down'></i> Fake</button>";
													echo "</div>";
											  echo "</form>";
											}
								  	}

										echo "<a id='overview' class='col-lg-12 btn btn-success' href='search.php'>Back to Overview</a>"; //Return to overview page
								echo "</div>";
				   echo "</div>";

				   echo "<div class='col-lg-8 order-lg-1 panel panel-default'>";
				   	echo "<div class='panel-heading'>";
				   		echo "<h2>Your Article:</h2>";
				   	echo "</div>";
				  	echo "<div class='panel-body'>";
					  	echo "<iframe src='".$_POST['url']."' width='100%' height='500' frameborder='0'>";
					  	echo "</iframe>";
							echo "<p><b>Not showing?</b><br/> Unfortunately this website has blocked previews for this article.</p>";
				 		echo "</div>";
					 echo "</div>";

				} else { //If URL is not valid post error
				  echo "<div class='card-div offset-lg-4 col-lg-4'>";
				    echo "<div class='card'>";
				      echo "<div class='card-body'>";
				        echo "<h5 class='card-title'>Invalid URL</h5>";
				        echo "<hr>";
				        echo "<p>Please enter a valid URL, for example: <span class='badge badge-info'>https://www.example.com</span></p>";
				        echo "<a class='col-lg-12 btn btn-success' href='search.php'>Back to Overview</a>";
				      echo "</div>";
				    echo "</div>";
				  echo "</div>";
 				}

		} else {
		   echo "<div class='row'>";
		     echo "<div class='card-div col-lg-4 col-sm-12'>";
		       echo "<div class='card'>";
		         echo "<div class='card-body'>";
		           echo "<h5 class='card-title'>Recently Resolved</h5>";
		           echo "<hr>";
		           echo "<div class='card-text'>";
							 		//Get top 5 most recent articles where a result has been decided
		              $sql = "SELECT URL, Legit, Fake FROM articles WHERE Closed = 1 ORDER BY DateResolved DESC LIMIT 5;";
		              $resolved = $conn->query($sql);

		                if ($resolved->num_rows > 0) {
		                    while($row = $resolved->fetch_assoc()) {
		                      echo "<div class='row'>";
			                      echo "<div class='col-lg-9'>";
															//Gets favicon from URL
				                      echo "<img class='fav' src='https://www.google.com/s2/favicons?domain_url=".$row['URL']."' alt='".$row["URL"]."' height='16' width='16'/>";
				                      echo "<p class='vote-url'>&nbsp".$row['URL']."</p>";
			                      echo "</div>";
			                      echo "<div class='decided vote col-lg-3'>";
				                      echo "<form method='post' action='search.php'>";
				                      echo "<input type='hidden' name='url' id='hiddenurl' value='".$row['URL']."'/>";
				                      if ($row['Legit'] > $row['Fake']) {
				                        echo "<button type='submit' class='view-post'><span class='col-lg-12 badge badge-success'>Trusted</span></button>";
				                      } else {
				                        echo "<button type='submit' class='view-post'><span class='col-lg-12 badge badge-danger'>Fake</span></button>";
				                      }
				                      echo "</form>";
			                      echo "</div>";
													echo "</div>";
		                      echo "<hr/>";
		                    }
		                	}

			          echo "</div>";
			       echo "</div>";
			      echo "</div>";
			    echo "</div>";

			    echo "<div class='card-div col-lg-4 col-sm-12'>";
			      echo "<div class='card'>";
			        echo "<div class='card-body'>";
			          echo "<h5 class='card-title'>Trending</h5>";
			          echo "<hr>";

								//Get most voted on URLs in the last week (closed or open)
		            $sql = "SELECT URL, Closed, Legit, Fake, (Legit + Fake) AS Total FROM articles WHERE DateResolved between date_sub(now(),INTERVAL 1 WEEK) and now() ORDER BY Total DESC LIMIT 5;";
		            $resolved = $conn->query($sql);

		              if ($resolved->num_rows > 0) {
		                  while($row = $resolved->fetch_assoc()) {
		                    echo "<div class='row'>";
			                    echo "<div class='col-lg-9'>";
				                    echo "<img class='fav' src='https://www.google.com/s2/favicons?domain_url=".$row['URL']."' alt='".$row["URL"]."' height='16' width='16'/>";
				                    echo "<p class='vote-url'>&nbsp".$row['URL']."</p>";
				                  echo "</div>";
			                    echo "<div class='vote decided col-lg-3'>";
				                    echo "<form method='post' action='search.php'>";
				                    echo "<input type='hidden' name='url' id='hiddenurl' value='".$row['URL']."'/>";
					                    if ($row['Closed'] == 0) {
					                      echo "<button type='submit' name='vote' value='fake' class='btn btn-info'><i class='fas fa-balance-scale'></i></button>";
					                  	} else {
						                    if ($row['Legit'] > $row['Fake']) {
						                      echo "<button type='submit' class='view-post'><span class='col-lg-12 badge badge-success'>Trusted</span></button>";
						                    } else {
						                      echo "<button type='submit' class='view-post'><span class='col-lg-12 badge badge-danger'>Fake</span></button>";
						                    }
					                  	}
				                    echo "</form>";
			                    echo "</div>";
												echo "</div>";
		                    echo "<hr/>";
		                  }
		              	}

			        echo "</div>";
			      echo "</div>";
			    echo "</div>";

			    echo "<div class='card-div col-lg-4 col-sm-12'>";
			      echo "<div class='card'>";
			        echo "<div class='card-body'>";
			          echo "<h5 class='card-title'>Latest</h5>";
			          echo "<hr>";

								//Get last 5 articles which are open to vote
		            $sql = "SELECT URL, Legit, Fake FROM articles WHERE Closed = 0 ORDER BY DateResolved DESC LIMIT 5;";
		            $resolved = $conn->query($sql);

		              if ($resolved->num_rows > 0) {
		                  while($row = $resolved->fetch_assoc()) {
		                    echo "<div class='row'>";
			                    echo "<div class='col-lg-9'>";
				                    echo "<img class='fav' src='https://www.google.com/s2/favicons?domain_url=".$row['URL']."' alt='".$row["URL"]."' height='16' width='16'/>";
				                    echo "<p class='vote-url'>&nbsp".$row['URL']."</p>";
			                    echo "</div>";
			                    echo "<div class='vote col-lg-3'>";
				                    echo "<form method='post' action='search.php'>";
				                      echo "<input type='hidden' name='url' id='hiddenurl' value='".$row['URL']."'/>";
				                      echo "<button type='submit' name='vote' value='fake' class='btn btn-info'><i class='fas fa-balance-scale'></i></button>";
				                    echo "</form>";
			                    echo "</div>";
												echo "</div>";
		                    echo "<hr/>";
		                  }
		              	}

			        echo "</div>";
			     echo "</div>";
			    echo "</div>";
			  echo "</div>";
		}

		$conn->close();
		?>
	</main>

	<footer class="footer">
		<p class="float-right"><a href="">Back to top</a></p>
		<p>© 2018 Benjamin P, Daniel E, Jake H, Jack P · <a href="#">Privacy</a> · <a href="#">Terms</a></p>
	</footer>

	<script type="text/javascript">
		$(document).ready(function() {
			$("body").tooltip({ selector: '[data-toggle=tooltip]' });
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#searchbar").val('<?php echo $_POST['url'] ?>');
		});
	</script>

	<!-- Bootstrpa Core JavaScript -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.4/js/all.js"></script>

</body>
</html>
